const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');
const expressValidator = require('express-validator');
const cors = require('cors');
const fs = require('fs');
const server = require('http').createServer(app);
const io = require('socket.io')(server)
dotenv.config();
const Chat = require('./models/Chat');
// DB
const connect = mongoose.connect(process.env.MONGO_URI, { useUnifiedTopology: true }).then(() => console.log("DB Connected"));

mongoose.connection.on('error', err => console.log(`DB Connection Error ${err.message}`));

io.on('connection', socket => {
    socket.on('Input Chat Message', msg => {
        connect.then(db => {
            try {
                let chat = new Chat({ message: msg.message, sender: msg.user._id, type: msg.type });
                chat.save((err, doc) => {
                    if (err) return res.json({ successs: false, err })

                    Chat.find({ '_id': doc._id })
                        .populate('sender')
                        .exec((err, doc) => {
                            return io.emit('Output Chat Message', doc)
                        })
                })
            }
            catch (error) {
                console.log(error)
            }
        })
    })
})

// Bring in routes
const postRoutes = require('./routes/post.routes.js');
const authRoutes = require('./routes/auth.routes.js');
const userRoutes = require('./routes/user.routes.js');
const chatRoutes = require('./routes/chat.routes.js');

const { json } = require('body-parser');

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());

// Routes
app.use("/", postRoutes);
app.use("/", authRoutes);
app.use("/", userRoutes);
app.use("/", chatRoutes);

// apiDocs
app.get('/api', (req, res) => {
    fs.readFile('docs/apiDocs.json', (err, data) => {
        if (err) {
            res.status(400).json(err);
        }
        const docs = JSON.parse(data);
        res.json(docs);
    });
});


app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({
            error: 'Unauthorized'
        })
    }
})

const port = process.env.PORT || 8080;
server.listen(port, () => console.log(`A Node Js API is listening on port: ${port}`));