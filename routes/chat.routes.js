const express = require('express');
const { getAllChats } = require('../controllers/chat.controller');
const { requireSignin } = require('../controllers/auth');

const router = express.Router();

router.get('/chats', requireSignin, getAllChats);

module.exports = router;
