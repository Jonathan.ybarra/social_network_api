const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const { ObjectId } = mongoose.Schema;

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true
    },
    photo_profile: {
        type: String,
        trim: true,
        required: false
    },
    photo_cover: {
        type: String,
        trim: true,
        required: false
    },
    age: {
        type: Number,
        trim: true,
        required: false
    },
    university: {
        type: String,
        trim: true,
        required: false
    },
    city_residence: {
        type: String,
        trim: true,
        required: false
    },
    company: {
        type: String,
        trim: true,
        required: false
    },
    link_profile_job: {
        type: String,
        trim: true,
        required: false
    },
    preference_one: {
        type: String,
        trim: true,
        required: false
    },
    preference_two: {
        type: String,
        trim: true,
        required: false
    },
    hashed_password: {
        type: String,
        required: true
    },
    salt: String,
    created: {
        type: Date,
        default: Date.now
    },
    updated: Date,
    about: {
        type: String,
        trim: true
    },
    following: [
        {
            type: ObjectId,
            ref: 'User'
        }
    ],
    followers: [
        {
            type: ObjectId,
            ref: 'User'
        }
    ],
    resetPasswordLink: {
        data: String,
        default: ""
    }
})

// Virtual field
userSchema.virtual('password')
    .set(function (password) {
        // Create temporaly variable called_paasword
        this._password = password;
        // Generate timestamp
        this.salt = uuidv4();
        // encrytpPassword
        this.hashed_password = this.encryptPassword(password);
    })
    .get(function () {
        return this._password
    });

// Methods
userSchema.methods = {
    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashed_password
    },
    encryptPassword: function (password) {
        if (!password) return '';

        try {
            return crypto.createHmac('sha1', this.salt)
                .update(password)
                .digest('hex');
        }
        catch (err) {
            return '';
        }
    }
}
module.exports = mongoose.model('User', userSchema);