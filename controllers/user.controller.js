const User = require('../models/user');
const _ = require('lodash');
const { result } = require('lodash');

exports.userById = (req, res, next, id) => {
    User.findById(id)
        .populate('following', '_id name photo_profile about email link_profile_job')
        .populate('followers', '_id name photo_profile about email link_profile_job')
        .exec((err, user) => {
            if (err || !user) {
                return res.status(400).json({
                    error: "User not found"
                })
            }

            req.profile = user; // adds profile object in req with user info
            next();
        })
}

exports.userPhotoProfile = (req, res) => {
    return res.json(req.profile.photo_profile);
}

exports.hasAuthorization = (res, req, next) => {
    const authorized = req.profile && req.auth && req.profile._id === req.auth._id;
    if (!authorized) {
        return res.status(403).json({ error: 'User is not authorized to perform this action' });
    }
}

exports.allUsers = (req, res) => {
    User.find((err, users) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json({ users });
    })
        .populate('following', '_id name photo_profile about email link_profile_job')
        .populate('followers', '_id name photo_profile about email link_profile_job')
}

exports.getUser = (req, res) => {
    return res.json(req.profile);
}

exports.updateUser = (req, res, next) => {
    let user = req.profile;
    user = _.extend(user, req.body); // extend - mutate the source object
    user.updated = Date.now()
    user.save((err) => {
        if (err) {
            return res.status(400).json({ error: 'You are not authorizarized to perform this a action' });
        }
        res.json({ user });
    })
}

exports.deleteUser = (req, res) => {
    let user = req.profile;
    user.remove((err, user) => {
        if (err) {
            return res.status(400).json({ error: err });
        }
        res.json({ message: 'User deleted successfully' });
    });
}

// follow unfollow
exports.addFollowing = (req, res, next) => {
    const { body } = req;

    User.findByIdAndUpdate(body.userId, {
        $push: { following: body.followId }
    },
        { new: true },
        (err, result) => {
            if (err) {
                return res.status(400).json({ error: err });
            }
            next();
        }
    )
}

exports.addFollowers = (req, res) => {
    const { body } = req;
    User.findByIdAndUpdate(body.followId, {
        $push: { followers: body.userId }
    },
        {
            new: true
        },
        (err, result) => {
            if (err) {
                return res.status(400).json({ error: err });
            }
            res.json(result);
        })
        .populate('following', '_id name photo_profile email')
        .populate('followers', '_id name photo_profile email')
}

// remove follow unfollow
exports.removeFollowing = (req, res, next) => {
    const { body } = req;
    User.findByIdAndUpdate(
        body.userId,
        { $pull: { following: body.unfollowId } },
        (err, result) => {
            if (err) {
                return res.status(400).json({ error: err });
            }
            next();
        })
}

exports.removeFollowers = (req, res) => {
    const { body } = req;
    User.findByIdAndUpdate(
        body.unfollowId,
        { $pull: { followers: body.userId } },
        { new: true }
    )
        .populate('following', '_id name')
        .populate('followers', '_id name')
        .exec((err, user) => {
            if (err || !user) {
                return res.status(400).json({
                    error: err
                })
            }
            result.hashed_password = undefined;
            result.salt = undefined;
            res.json(result);
        })
}

exports.deleteFollower = (req, res) => {
    const { body } = req;
    User.findByIdAndUpdate(
        body.userId,
        { $pull: { followers: body.unfollowId } },
        { new: true }
    )
        .exec((err, user) => {
            if (err || !user) {
                return res.status(400).json({
                    error: err
                })
            }
            result.hashed_password = undefined;
            result.salt = undefined;
            console.log(result);
        })

    User.findByIdAndUpdate(
        body.unfollowId,
        { $pull: { following: body.userId } },
        (err, result) => {
            if (err) {
                return res.status(400).json({ error: err });
            }
            res.json(result);
        })

}

exports.findPeople = (req, res) => {
    const { profile } = req;
    let following = profile.following;

    following.push(profile._id);

    User.find({ _id: { $nin: following } }, (err, users) => {
        if (err) {
            return res.status(400).json({
                error: err
            });
        }
        res.json(users);
    })
        .select('name')
}

// Search
exports.searchUsers = (req, res) => {
    const { name } = req.query;

    User.find({
        name: {
            $regex: new RegExp(name)
        }
    },
        (err, users) => {
            res.json(users);
        }
    )
}